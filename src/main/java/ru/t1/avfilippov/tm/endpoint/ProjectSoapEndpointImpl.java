package ru.t1.avfilippov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.avfilippov.tm.api.service.ProjectDTOService;
import ru.t1.avfilippov.tm.entity.dto.ProjectDto;
import ru.t1.avfilippov.tm.entity.soap.*;

@Endpoint
public class ProjectSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://avfilippov.t1.ru/tm/entity/soap";

    @Autowired
    private ProjectDTOService service;

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        final ProjectDeleteByIdResponse response = new ProjectDeleteByIdResponse();
        final String id = request.getId();
        service.deleteById(id);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse clear(@RequestPayload final ProjectClearRequest request) {
        final ProjectClearResponse response = new ProjectClearResponse();
        service.clear();
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) {
        final ProjectSaveResponse response = new ProjectSaveResponse();
        final ProjectDto project = request.getProjectDto();
        service.save(project);
        response.setProjectDto(project);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        final ProjectFindAllResponse response = new ProjectFindAllResponse();
        response.setProjectDto(service.findAll());
        return response;
    }

}
