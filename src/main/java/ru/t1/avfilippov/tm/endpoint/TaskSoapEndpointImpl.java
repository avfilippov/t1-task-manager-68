package ru.t1.avfilippov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.avfilippov.tm.api.service.TaskDTOService;
import ru.t1.avfilippov.tm.entity.dto.TaskDto;
import ru.t1.avfilippov.tm.entity.soap.*;

public class TaskSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public final static String NAMESPACE = "http://avfilippov.t1.ru/tm/entity/soap";

    @Autowired
    private TaskDTOService service;

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) {
        final TaskDeleteByIdResponse response = new TaskDeleteByIdResponse();
        final String id = request.getId();
        service.deleteById(id);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskClearRequest", namespace = NAMESPACE)
    public TaskClearResponse clear(@RequestPayload final TaskClearRequest request) {
        final TaskClearResponse response = new TaskClearResponse();
        service.clear();
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) {
        final TaskSaveResponse response = new TaskSaveResponse();
        final TaskDto task = request.getTaskDto();
        service.save(task);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) {
        final TaskFindAllResponse response = new TaskFindAllResponse();
        response.setTaskDto(service.findAll());
        return response;
    }

}
