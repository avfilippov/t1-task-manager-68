package ru.t1.avfilippov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.avfilippov.tm.api.endpoint.TaskEndpoint;
import ru.t1.avfilippov.tm.api.service.TaskDTOService;
import ru.t1.avfilippov.tm.entity.dto.TaskDto;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpointImpl implements TaskEndpoint {

    @Autowired
    private TaskDTOService service;

    @Override
    @GetMapping("/findAll")
    public List<TaskDto> findAll() {
        return service.findAll();
    }

    @Override
    @PostMapping("/save")
    public TaskDto save(@RequestBody final TaskDto task) {
        return service.save(task);
    }

    @Override
    @GetMapping("/findById/{id}")
    public TaskDto findById(@PathVariable("id") String id) {
        return service.findById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") String id) {
        return service.existsById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return service.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") String id) {
        service.deleteById(id);
    }

    @Override
    @PostMapping("/delete")
    public void deleteById(@RequestBody TaskDto task) {
        service.delete(task);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteById(@RequestBody List<TaskDto> tasks) {
        service.deleteAll(tasks);
    }

    @Override
    @PostMapping("/clear")
    public void clear() {
        service.clear();
    }

}
