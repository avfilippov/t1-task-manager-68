package ru.t1.avfilippov.tm.config;

import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.endpoint.ProjectSoapEndpointImpl;
import ru.t1.avfilippov.tm.endpoint.TaskSoapEndpointImpl;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@EnableWs
@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.avfilippov.tm")
@EnableJpaRepositories("ru.t1.avfilippov.tm.repository")
public class ApplicationConfiguration extends WsConfigurerAdapter {

    @Autowired
    private IPropertyService propertyService;

    @Bean
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDatabaseDriver());
        dataSource.setUrl(propertyService.getDatabaseUrl());
        dataSource.setUsername(propertyService.getDatabaseUser());
        dataSource.setPassword(propertyService.getDatabasePassword());
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(final DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.avfilippov.tm");
        final Properties properties = new Properties();
        properties.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getDatabase2ddlAuto());
        properties.put(Environment.SHOW_SQL, propertyService.getDatabaseShowSql());
        properties.put(Environment.FORMAT_SQL, propertyService.getDatabaseFormatSql());
        properties.put(Environment.DEFAULT_SCHEMA, propertyService.getDatabaseSchemaName());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }


    private Connection getConnection(Properties properties) throws SQLException {
        return DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
    }


    @Bean
    public PlatformTransactionManager transactionManager(final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }


    @Bean(name = "ProjectEndpoint")
    public DefaultWsdl11Definition projectWsdlDefinition(XsdSchema projectEndpointSchema) {
        final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(ProjectSoapEndpointImpl.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(ProjectSoapEndpointImpl.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(ProjectSoapEndpointImpl.NAMESPACE);
        wsdl11Definition.setSchema(projectEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "TaskEndpoint")
    public DefaultWsdl11Definition taskWsdlDefinition(XsdSchema taskEndpointSchema) {
        final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(TaskSoapEndpointImpl.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(TaskSoapEndpointImpl.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(TaskSoapEndpointImpl.NAMESPACE);
        wsdl11Definition.setSchema(taskEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "projectEndpointSchema")
    public XsdSchema projectEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectEndpoint.xsd"));
    }

    @Bean(name = "taskEndpointSchema")
    public XsdSchema taskEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/taskEndpoint.xsd"));
    }
}
