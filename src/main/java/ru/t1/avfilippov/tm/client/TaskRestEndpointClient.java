package ru.t1.avfilippov.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.avfilippov.tm.entity.dto.TaskDto;

import java.util.List;

public interface TaskRestEndpointClient {

    public static final String BASE_URL = "http://localhost:8080/api/tasks/";

    static TaskRestEndpointClient client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class,BASE_URL);
    }

    @GetMapping("/findAll")
    List<TaskDto> findAll();

    @PostMapping("/save")
    TaskDto save(@RequestBody TaskDto task);

    @GetMapping("/findById/{id}")
    TaskDto findById(@PathVariable("id") String id);

    @GetMapping("/exsitsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/delete")
    void delete(@RequestBody TaskDto task);

    @PostMapping("/deleteAll")
    void deleteAll(@RequestBody List<TaskDto> tasks);

    @PostMapping("/clear")
    void clear();

}
