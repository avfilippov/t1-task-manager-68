package ru.t1.avfilippov.tm.entity.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.avfilippov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@Entity
@Table(name = "tm_project")
@NoArgsConstructor
public class ProjectDto {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    private String name;

    @Column
    private String description;

    @Column
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart = new Date();

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    public ProjectDto(final String name) {
        this.name = name;
    }

}
