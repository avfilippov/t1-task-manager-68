package ru.t1.avfilippov.tm.api.service;

public interface IPropertyService {

    String getDatabaseDriver();

    String getDatabaseUrl();

    String getDatabaseUser();

    String getDatabasePassword();

    String getDatabaseDialect();

    String getDatabase2ddlAuto();

    String getDatabaseShowSql();

    String getDatabaseFormatSql();

    String getDatabaseSchemaName();

}
